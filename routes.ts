import { getAlerts } from './controllers/alertsController.ts';

import { oak } from "./deps.ts"

const router = new oak.Router()

router.get("/alerts", getAlerts)

export default router


