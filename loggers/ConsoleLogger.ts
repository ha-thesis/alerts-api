import { Log } from "../models/Log.ts";
import { ILogger } from './ILogger.ts';


export class ConsoleLogger implements ILogger {
    log(log: Log): void {
        console.log(`[${log.getStartDateTime().toLocaleString()}] ${log.getClientIP()} - ${log.getMethod()} ${log.getStatus()} ${log.getDuration()}ms ${log.getEndpoint()}`);
    }

    logString(log: string): void {
        console.log(`[${new Date().toLocaleString()}] ${log}`);
    }
}
