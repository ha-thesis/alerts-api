import { Log } from "../models/Log.ts";
import { ILogger } from './ILogger.ts';
export default (loggers: ILogger[]) => {
        return async (
        // deno-lint-ignore no-explicit-any
        { response, request}: { response: any, request: any},
        next: () => Promise<unknown>,
    ) => {
        const log = new Log(request.method, request.url.pathname);
        log.setClientIP(request.ip); 
        await next();
        log.finish(response.status, "");
        loggers.forEach((logger) => logger.log(log));
    }
};
