import { Log } from "../models/Log.ts";

export interface ILogger {
    log(log: Log): void;
}