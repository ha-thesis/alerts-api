import { ConsoleLogger } from "./loggers/ConsoleLogger.ts";
import { cockroachDBService } from "./services/cockroachDB.ts";
import { oak } from "./deps.ts";
import { ILogger } from "./loggers/ILogger.ts";
import router from "./routes.ts";
import httpLogger from "./loggers/httpLogger.ts";

function getEnvVariable(variableName: string): string {
    const envVariable = Deno.env.get(variableName);
    if (envVariable === undefined) {
        throw new Error(`Environment variable ${variableName} is not set`);
    }
    return envVariable;
}

const consoleLogger: ConsoleLogger = new ConsoleLogger();
const httpServer = new oak.Application();

const cockroachDBHost = getEnvVariable("COCKROACHDB_HOST");
const cockroachDBPort = Number(getEnvVariable("COCKROACHDB_PORT"));
const cockroachDBUsername = getEnvVariable("COCKROACHDB_USERNAME");
const cockroachDBPassword = getEnvVariable("COCKROACHDB_PASSWORD");
const cockroachDBDatabase = getEnvVariable("COCKROACHDB_DATABASE");
const cockroachdb = new cockroachDBService(cockroachDBHost, cockroachDBPort, cockroachDBUsername, cockroachDBPassword, cockroachDBDatabase, "ca.crt");
await cockroachdb.connect();

const loggers: ILogger[] = [consoleLogger];

httpServer.use(cockroachdb.oakUseWrapper)
httpServer.use(httpLogger(loggers))
httpServer.use(router.routes());
httpServer.use(router.allowedMethods());

httpServer.listen({ port: 8080 });
consoleLogger.logString("Server running on port 8080");