FROM denoland/deno:alpine-1.28.0 as builder
WORKDIR /app
COPY deps.ts lock.json ./
RUN deno cache --lock lock.json deps.ts
ADD . .
RUN deno compile --output alerts-api --allow-net --allow-run --allow-read --allow-env main.ts

FROM frolvlad/alpine-glibc:alpine-3.16 as runner
LABEL org.opencontainers.image.authors mrpsycho@mrpsycho.pl
EXPOSE 8080 
WORKDIR /app
RUN adduser -D demo-app \
    && chown -R demo-app /app 
USER demo-app
COPY --from=builder /app/alerts-api /app/alerts-api
COPY ca.crt .
CMD ["/bin/sh", "-c", "/app/alerts-api"]








