import { cloneState } from "https://deno.land/x/oak@v11.1.0/structured_clone.ts";

// deno-lint-ignore no-explicit-any
export const getAlerts = async (ctx: any) => {      
    const alerts = await ctx.state.dbClient.getAlerts();
    ctx.response.headers.set("Access-Control-Allow-Origin", "*");
    ctx.response.status = 200;
    ctx.response.body = alerts;
    return;
}