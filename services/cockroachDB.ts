import { postgres } from "../deps.ts";
import { ConsoleLogger } from "../loggers/ConsoleLogger.ts";
import { AlertFromDB } from "../models/AlertFromDB.ts";
import { Alert } from "../models/Alerts.ts.ts";

export class cockroachDBService {
    private host: string;
    private port: number;
    private user: string;
    private password: string;
    private database: string;
    private caCrtFileLocation: string;
    private logger: ConsoleLogger;

    public client: postgres.Client|undefined;

    constructor(host: string, port: number, user: string, password: string, database: string, caCrtFileLocation: string) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database = database;
        this.caCrtFileLocation = caCrtFileLocation;
        this.logger = new ConsoleLogger();

    }

    oakUseWrapper = async (ctx: any, next: any) => {
        ctx.state.dbClient = this;
        await next();
    }

    private readCertificate(caCrtFileLocation: string): string {
        const decoder = new TextDecoder();
        const data = Deno.readFileSync(caCrtFileLocation);
        const certificate = decoder.decode(data);
        return certificate;
    }

    public async connect() {
        const certificate = this.readCertificate(this.caCrtFileLocation);
        const tlsOptions: postgres.TLSOptions = {
            enabled: true,
            enforce: true,
            caCertificates: [certificate],
        };
        try {
            const client = new postgres.Client({
                user: this.user,
                database: this.database,
                hostname: this.host,
                port: this.port,
                password: this.password,
                tls: tlsOptions
            });
            await client.connect();
            this.logger.logString("Connected to CockroachDB");
            this.client = client;
            return client;
        } catch (error) {
            console.log(error);
            setTimeout(() => {
                Deno.exit(1);
            }, 1000);
        }
    }

    public test() {
        console.log("test");
    }

    
    public async getAlerts(): Promise<Alert[]> {
        const alerts: Alert[] = [];
        if (this.client === undefined) {
            throw new Error("Client is undefined");
        }

        const { rows } = await this.client.queryObject<AlertFromDB>("SELECT * FROM alerts");
        if (rows.length === 0) {
            return alerts;
        }

        for (const row of rows) {
            alerts.push(new Alert(Number(row.id), row.deviceid, Number(row.avergepulse), Number(row.currentpulse), new Date(row.datetimestarted)));
        }

        return alerts;

    }
}