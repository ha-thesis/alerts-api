export class Log {
    private startDateTime: Date;
    private endDateTime: Date | undefined;
    private method: string;
    private endpoint: string;
    private status = -1;
    private clientIP!: string;
    private message: string | undefined;


    constructor(method: string, endpoint: string) {
        this.startDateTime = new Date();
        this.method = method;
        this.endpoint = endpoint;
    }

    finish(status: number, message: string): void {
        this.endDateTime = new Date();
        this.status = status;
        this.message = message;
    }

    getStartDateTime(): Date {
        return this.startDateTime;
    }

    getEndDateTime(): Date | undefined {
        return this.endDateTime;
    }

    getMethod(): string {
        return this.method;
    }

    getEndpoint(): string {
        return this.endpoint;
    }

    getStatus(): number {
        return this.status;
    }

    getMessage(): string {
        if (this.message === undefined) {
            return "";
        }
        return this.message;
    }

    setClientIP(clientIP: string): void {
        this.clientIP = clientIP;
    }

    getClientIP(): string {
        return this.clientIP;
    }

    getDuration(): number {
        if (this.endDateTime === undefined) {
            return -1;
        }
        return this.endDateTime.getTime() - this.startDateTime.getTime();
    }

}