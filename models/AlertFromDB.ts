export class AlertFromDB {
    id: BigInt
    deviceid: string
    avergepulse: number
    currentpulse: BigInt
    datetimestarted: Date

    constructor(id: BigInt, deviceid: string, averagepulse: number, currentpulse: BigInt, datetimestarted: Date) {
        this.id = id
        this.deviceid = deviceid
        this.avergepulse = averagepulse
        this.currentpulse = currentpulse
        this.datetimestarted = datetimestarted
    }

}