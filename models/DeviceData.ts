export class DeviceData {
    deviceId: string;
    pulse: number;

    constructor(deviceId: string, pulse: number) {
        this.deviceId = deviceId;
        this.pulse = pulse;
    }
    
}