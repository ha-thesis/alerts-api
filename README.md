### Env variables

  COCKROACHDB_HOST
  COCKROACHDB_PORT
  COCKROACHDB_USERNAME
  COCKROACHDB_PASSWORD
  COCKROACHDB_DATABASE

If you are using VSCode's launch feature, just create `.env` file in the root of the project and add the above variables.